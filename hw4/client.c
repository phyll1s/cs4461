// Andrew Markiewicz
// HTTP Client - Computer Networks


#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>

#include "common.h"

int main(int argc, char *argv[]){
    //Check for correct number of arguments
    if(argc != 3){
        printf("Error - wrong number of arguments\n");
        exit(EXIT_FAILURE);
    }
    
    /* Setup socket. */
    int sock = common_createSocket(argv[1], argv[2]);
    FILE *stream = common_getStream(sock);
    size_t error;
    char toWrite[100+strlen(argv[1])];
    size_t writeSize;
    sprintf(toWrite,"GET / HTTP/1.1\nHost: %s\nConnection: close%s",argv[1],"\r\n\r\n");
    
    writeSize = strlen(toWrite);
    printf("\n##### CLIENT IS SENDING THE FOLLOWING TO SERVER:\n%s\n",toWrite);    
    error = fwrite(toWrite,writeSize,1,stream);
    
    // while there is data to read
    printf("##### CLIENT RECEIVED THE FOLLOWING FROM SERVER:\n");
    while(1){
	    char *string = NULL;
	    int len = common_getLine(&string, stream);
        if(len == -1){
            if(fclose(stream) != 0){
                printf("Error fclose() - %s\n",strerror(errno));
                exit(EXIT_FAILURE);
            }
            printf("##### Connection closed by server\n");
            exit(EXIT_SUCCESS);
        }
        
        //Print line
        printf("%s",string);
        
        //Detect chunked encoding
        if(strcmp(string,"Transfer-Encoding: chunked\r\n")==0)
            printf("##### Detected chunked transfer encoding\n");
        
        //Detect end of headers   
        if(strcmp(string,"\r\n")==0){
            free(string);
            
    	    char *string = NULL;
	        int len = common_getLine(&string, stream);
            
        }
        
        
	    free(string);
    }    
}
