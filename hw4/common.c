// Andrew Markiewicz
// Computer Networks

#include "common.h"
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>


/* Number of incoming connections to queue up while we are answering other
 * requests.  Used when we call listen(). */
#define LISTEN_QUEUE_SIZE 10


/* common_getLine() Reads a line from a stream and stores it in "string"
 * which will automatically be appropriately allocated.
 *
 * RETURNS: Number of characters in the line and stores the line in the
 * variable "string".  This may differ from strlen(string) if the string
 * contains \0.  The "string" variable should be free()'d by the caller.
 * Returns -1 when the end of the stream is reached.  Exits if some other
 * error occurs.
 *
 * You should call this function this way:
 * char *string = NULL;
 * common_getLine(&string, stream);
 *
*/
size_t common_getLine(char **string, FILE *stream){
    size_t origSize = 0;
    size_t toReturn; 
    	    
    toReturn = getline(string,&origSize,stream);
    if(toReturn == -1){
        if(feof(stream) != 0){
            return -1;
        }else{
            printf("Error getline()\n");
            exit(EXIT_FAILURE);
        }
    }
    
    return toReturn;
}



/* Convert file descriptor into stream (i.e., FILE*).  Exit if there is a
 * failure. */
FILE* common_getStream(int sock){
    FILE* toReturn;
    toReturn = fdopen(sock,"a+b");
    if(toReturn == NULL){
        printf("Error fdopen() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    return toReturn;
}


/* Given a socket file descriptor for a server, this function gets a new
 * socket for a specific incoming connection.  If no incoming connections
 * are present, this function will block. */
int common_acceptConnect(int sock)
{
    // TODO:
    // Call accept(sock, NULL, NULL)
    // Check for errors. If one occurred, try again repeatedly!
    // Return socket/file descriptor that accept() created.

    return 1;
}


/* Creates a TCP socket and returns the file descriptor for it.  If
 * hostname==NULL, will create a socket listening on port. Exits if there
 * is an error.*/
int common_createSocket(char *hostname, char *port){
    if(hostname == NULL && port == NULL){
	    fprintf(stderr, "Both arguments to common_createSocket() were NULL\n");
	    exit(EXIT_FAILURE);
    }

    // Request a TCP connection using the version of IP that is configured
    // on the machine.
    struct addrinfo * result;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE|AI_ADDRCONFIG;

    //Call getaddrinfo to populate results
    int errorNum;
    errorNum = getaddrinfo(hostname,port,&hints,&result);
    if(errorNum != 0){
        printf("Error getaddrinfo() - %s\n",gai_strerror(errorNum));
        exit(EXIT_FAILURE);
    }
    
    //Create a socket
    int sock;
    sock = socket(result->ai_family,result->ai_socktype,0);
    if(sock == -1){
        printf("Error socket() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* If we are the client, initiate the connection. */
    if(hostname != NULL){
        int tempError;
        tempError = connect(sock,result->ai_addr,(socklen_t)result->ai_addrlen);
        if(tempError == -1){
            printf("Error connect() - %s\n",strerror(errno));
            exit(EXIT_FAILURE);
        }
    }
    
    // If we are the server
    else{
	    /* Prevent the server from getting stuck in the TIME_WAIT state
	     * even though the server has been killed. */
	    int yes = 1;
	    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) != 0){
	        perror("server: setsockopt() failed: ");
	        exit(EXIT_FAILURE);
	    }
        
        //Binds to socket
	    int tempError;
	    tempError = bind(sock,result->ai_addr,(socklen_t)result->ai_addrlen);
        if(tempError == -1){
            printf("Error bind() - %s\n",strerror(errno));
            exit(EXIT_FAILURE);
        }
	
        //Listens on socket
	    int tempError1;
	    tempError1 = listen(sock,LISTEN_QUEUE_SIZE);
        if(tempError1 == -1){
            printf("Error bind() - %s\n",strerror(errno));
            exit(EXIT_FAILURE);
        }

    }  // end server-specific stuff

    freeaddrinfo(result);
    return sock;
}
