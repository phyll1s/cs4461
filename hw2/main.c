// Andrew Markiewicz
// CS4461 - Computer Networks

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>


/* PART 1: Check endianness and print message about endianness here. */
void checkEndian(){
    
    printf("Networks use big endian ordering.\n");
    
    /*Union with long and char allows you to access the indv bytes of the long int*/
    union {
        uint32_t num;
        char chr[4];
    } tempUnion;        
    tempUnion.num = 1;

    if(tempUnion.chr[3] && !tempUnion.chr[2] &&
        !tempUnion.chr[1] && !tempUnion.chr[0]){
        printf("This host uses big endian (most significant byte first).\n\n");
    }
    else if( !tempUnion.chr[3] && !tempUnion.chr[2] &&
        !tempUnion.chr[1] && tempUnion.chr[0] ){
        printf("This host uses little endian (least significant byte first).\n\n");
    }else{
        printf("This host is neither little nor big endian.\n\n");
    }
    
    return; 
}



int main(int argc, char *argv[]){
    
    /*Checks if user passed in appropriate number of arguments*/
    if(argc != 2){
        printf("Error - wrong number of arguments.\n");
        exit(1);
    }

    //Checks for endianness
    checkEndian();

    /*Creats hints struct to define type of address to get back*/
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    

    /* SOCK_STREAM=TCP, SOCK_DGRAM=UDP */
    hints.ai_socktype = SOCK_STREAM;

    /* AF_INET=IPv4, AF_INET6=IPv6, don't set at all
       to ask for either.  AF stands for "address family". */
    // hints.ai_family = AF_INET;  // LEAVE THIS COMMENTED OUT

    /* If AI_ADDRCONFIG is used, you will only get IPvX addresses returned
     * if IP version X is configured on your computer.  In the case of the
     * lab computers (which only support IPv4---and do not have IPv6
     * addresses), if you use ADDRCONFIG, you will not get to see any IPv6
     * addresses even if the website you are querying supports it. */
    // hints.ai_flags = AI_ADDRCONFIG;  // LEAVE THIS COMMENTED OUT


    struct addrinfo *answer;
    int error = getaddrinfo(argv[1], NULL, &hints, &answer);
    if(error != 0) {
	    fprintf(stderr, "Error - getaddrinfo(): %s\n", gai_strerror(error));
	    exit(1);
    }
    
    while(answer != NULL){
    
        //Prints IP address
        char* ipAddr;
        int tempLen,offset;
        
        /*For some reason NI_MAXHOST is not defined even though the appropriate
            libraries are included - using 1025 instead (what it is (usually) defined as)*/
        char reverseHostName[1025];
        
        /*Checks for IP version, prints accordingly*/
        /*IPV4*/
        if(answer->ai_family == AF_INET){
            printf("ipv4: ");
            ipAddr = malloc(INET_ADDRSTRLEN);
            /*Converts binary ai_addr to a printable form*/
            if(inet_ntop(answer->ai_family,answer->ai_addr->sa_data+2,ipAddr,INET_ADDRSTRLEN) == NULL)
                printf("Error - inet_ntop(): %\n",strerror(errno));
            
            struct sockaddr_in sai;
            memset(&sai,0,sizeof(sai));
            memcpy(&sai,answer->ai_addr,sizeof(sai));
            
            /*Does reverse dns lookup*/ 
            int error1;
            error1 = getnameinfo((struct sockaddr *)&sai,sizeof(sai),reverseHostName,sizeof(reverseHostName),NULL,0,0);
            if(error1){
                printf("Error - getnameinfo(): %i",error1);
                exit(1);
            }
        /*IPV6*/
        }else if(answer->ai_family == AF_INET6){
            printf("ipv6: ");
            ipAddr = malloc(INET6_ADDRSTRLEN);
            /*Converts binary ai_addr to a printable form*/
            if(inet_ntop(answer->ai_family,answer->ai_addr->sa_data+6,ipAddr,INET6_ADDRSTRLEN) == NULL)
                printf("Error - inet_ntop(): %\n",strerror(errno));
                
            struct sockaddr_in6 sai;
            memset(&sai,0,sizeof(sai));
            memcpy(&sai,answer->ai_addr,sizeof(sai));    
                
            /*Does reverse dns lookup*/ 
            int error1;
            error1 = getnameinfo((struct sockaddr *)&sai,sizeof(sai),reverseHostName,sizeof(reverseHostName),NULL,0,0);
            if(error1){
                printf("Error - getnameinfo(): %i",error1);
                exit(1);
            }
            
        /*Unkown AI_FAMILY*/
        }else{
            printf("Error - unknown IP version");
        }        

        printf("%s reverse DNS look up: %s\n",ipAddr,reverseHostName);
        free(ipAddr);
        
        /*Advances to next ip if there is one, freeing old space*/
        void* temp = answer;
        answer = answer->ai_next;
        free(temp);
    }
    freeaddrinfo(answer);
    return 0;
}
