#include "unreliable.h"

/* Put any functions here which you want to be
   able to use by both client.c and server.c. */


//Function used to create a checksum of a packet
int checksum(char* packet){
    int i,total;
    for(i=0;i<strlen(packet);i++){
        total = (total + packet[i]);   
    }    
    return total;
}


//Function used to format a packet
//Caller must free packet unless error
char* createPacket(int syn, int fin, int ack, int seqNum, int msgLen, char msg){
    char* packet = malloc(6+sizeof(seqNum)+5);
    
    //If no message, set msg to X
    if(msgLen == 0){
        msg = 'X';
    }
    
    //format initial string
    sprintf(packet,"%i:%i:%i:%i:%i:%c",syn,fin,ack,seqNum,msgLen,msg);
    
    //find checksum
    int checkSum = checksum(packet);
    char* checkSumString = malloc(sizeof(checkSum)+1);
    if(checkSumString == NULL){
        printf("Error malloc() - error malloc'ing in createPacket()\n");
        free(packet);
        return NULL;
    }
    sprintf(checkSumString,":%i",checkSum); 
    packet = realloc(packet,sizeof(packet)+strlen(checkSumString)+2);
    if(packet == NULL){
        printf("Error realloc() - error realloc'ing in createPacket()\n");
        free(packet);
        return NULL;
    }
    
    return strcat(packet,checkSumString);
}
