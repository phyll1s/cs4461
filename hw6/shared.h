#include "unreliable.h"

/* Put the prototypes here for any functions here which you want to be able
   to use by both client.c and server.c. */


// the UDP port users will be connecting to on server
#define SERVERPORT "6666"

//Function used to format a packet
char* createPacket(int, int, int, int, int, char);

//Function used to create a checksum of a packet
int checksum(char*);
